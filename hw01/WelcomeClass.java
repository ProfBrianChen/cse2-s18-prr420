//Pritam Reddy
//hw01

public class WelcomeClass{ //start public class
  /*
  -----------
  | WELCOME |
  -----------
  ^  ^  ^  ^  ^  ^
 / \/ \/ \/ \/ \/ \
<-E--J--K--0--0--0->
 \ /\ /\ /\ /\ /\ /
  v  v  v  v  v  v
  */
  public static void main(String[] args){ //start main method
    System.out.println("  -----------");
    System.out.println("  | WELCOME |");
    System.out.println("  -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-P--R--R--4--2--0->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");                 
  } //end main method
} //end public class