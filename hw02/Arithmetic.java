//Pritam Reddy
//hw02

public class Arithmetic{ //start public class
  public static void main(String[] args){ //start main method
    //Number of pairs of pants
  int numPants = 3;
  //Cost per pair of pants
  double pantsPrice = 34.98;

  //Number of sweatshirts
  int numShirts = 2;
  //Cost per shirt
  double shirtPrice = 24.99;

  //Number of belts
  int numBelts = 1;
  //cost per belt
  double beltCost = 33.99;

  //the tax rate
  double paSalesTax = 0.06;
    
    //total cost of each kind of item
    double totalCostOfPants; double totalCostOfShirts; double totalCostOfBelts;
    //sales tax of each kind of item
    double salesTaxOfPants; double salesTaxOfShirts; double salesTaxOfBelts;
    double totalCostOfPurchase; //total before sales tax
    double totalSalesTax; //total sales tax
    double totalPaid; //total including sales tax
    
    //calculations using defined variables
    totalCostOfPants=pantsPrice*numPants;
    totalCostOfShirts=shirtPrice*numShirts;
    totalCostOfBelts=beltCost*numBelts;
    salesTaxOfPants=(int)(paSalesTax*totalCostOfPants*100)/100.0;
    salesTaxOfShirts=(int)(paSalesTax*totalCostOfShirts*100)/100.0;
    salesTaxOfBelts=(int)(paSalesTax*totalCostOfBelts*100)/100.0;
    totalCostOfPurchase=totalCostOfBelts+totalCostOfShirts+totalCostOfPants;
    totalSalesTax=(int)((salesTaxOfBelts+salesTaxOfShirts+salesTaxOfPants)*100)/100.0;
    totalPaid=totalCostOfPurchase+totalSalesTax;

    //print out values
    System.out.println("Total cost of pants: $"+ totalCostOfPants);
    System.out.println("Total cost of shirts: $"+totalCostOfShirts);
    System.out.println("Total cost of belts: $"+totalCostOfBelts );
    System.out.println("Sales tax of pants: $"+salesTaxOfPants);
    System.out.println("Sales tax of shirts: $"+salesTaxOfShirts);
    System.out.println("Sales tax of belts: $"+salesTaxOfBelts);
    System.out.println("Total cost: $"+totalCostOfPurchase );
    System.out.println("Total Sales tax: $"+totalSalesTax);
    System.out.println("Total paid: $"+ totalPaid);

  } //end main method
} //end public class